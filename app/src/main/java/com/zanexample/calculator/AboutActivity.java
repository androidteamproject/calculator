package com.zanexample.calculator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AboutActivity extends Activity {
    private int oneNumber,
                twoNumber,
                result;
    TextView resultView;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        oneNumber = getIntent().getIntExtra(MainActivity.ONE_NUMBER, 0);
        twoNumber = getIntent().getIntExtra(MainActivity.TWO_NUMBER, 0);
        result = getIntent().getIntExtra(MainActivity.RESULT, 0);
        resultView = (TextView) findViewById(R.id.result);

        if(twoNumber<0){
            resultView.setText(Integer.toString(oneNumber) + "+" + "(" + Integer.toString(twoNumber) + ")" + "=" + Integer.toString(result));
        }else{
            resultView.setText(Integer.toString(oneNumber) + "+" + Integer.toString(twoNumber) + "=" + Integer.toString(result));
        }
    }
}