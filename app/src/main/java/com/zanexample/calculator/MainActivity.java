package com.zanexample.calculator;

/**
 * Проект: "Калькулятор"
 * Описание:
 * Приложение должно состоять из двух Activity, на первой Activity пользователь вводит
 * первое слагаемое, второе слагаемое и нажимает на копку “Сложить”, на втором Activity
 * пользователь видит текстовое сообщение вида “[ПЕРВОЕ_СЛАГАЕМОЕ] +
 * [ВТОРОЕ_СЛАГАЕМОЕ] = [СУММА]”.
 */

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    public Button buttonConsider;
    public EditText oneEditText,
                    twoEditText;
    public int oneNumber,
               twoNumber;
    public static final String ONE_NUMBER = "com.zanexample.calculator.ONE_NUMBER",
                                TWO_NUMBER = "com.zanexample.calculator.TWO_NUMBER",
                                RESULT = "com.zanexample.calculator.RESULT";
    public  Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        oneEditText = (EditText) findViewById(R.id.oneEditText);
        twoEditText = (EditText) findViewById(R.id.twoEditText);

        if(savedInstanceState!= null){
            oneNumber = savedInstanceState.getInt(ONE_NUMBER);
            twoNumber = savedInstanceState.getInt(TWO_NUMBER);

            oneEditText.setText(Integer.toString(oneNumber));
            twoEditText.setText(Integer.toString(twoNumber));
        }

        buttonConsider = (Button) findViewById(R.id.button_consider);
        buttonConsider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               if (validateEditText(oneEditText) &&
                       validateEditText(twoEditText)){

                    oneNumber = Integer.parseInt(oneEditText.getText().toString());
                    twoNumber = Integer.parseInt(twoEditText.getText().toString());

                    intent = new Intent(MainActivity.this, AboutActivity.class);
                    intent.putExtra(ONE_NUMBER, oneNumber);
                    intent.putExtra(TWO_NUMBER, twoNumber);
                    intent.putExtra(RESULT, oneNumber + twoNumber);
                    startActivity(intent);
                    overridePendingTransition(R.anim.diagonaltranslate, R.anim.alpha);

                }
            }
        });
    }

    public boolean validateEditText(EditText editText ){
            if(editText.getText().toString().equals("")){
                editText.setError(getString(R.string.error_empty_field));
                return false;
            }
            if(editText.getText().toString().equals("-")){
                editText.setError(getString(R.string.error_incorrect));
                return false;
            }
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle saveInstanceState){
        saveInstanceState.putInt(ONE_NUMBER, oneNumber);
        saveInstanceState.putInt(TWO_NUMBER, twoNumber);
        super.onSaveInstanceState(saveInstanceState);
    }
}

